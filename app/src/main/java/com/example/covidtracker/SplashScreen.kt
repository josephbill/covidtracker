package com.example.covidtracker

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.postDelayed


class SplashScreen : AppCompatActivity() {
    val SPLASH_DISPLAY_LENGTH: Int = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        //handler class

        //handler class
        Handler().postDelayed(Runnable { //intent
            val intent = Intent(this@SplashScreen, MainActivity::class.java)
            startActivity(intent)
        }, SPLASH_DISPLAY_LENGTH.toLong())
    }
}