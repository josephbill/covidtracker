package com.example.covidtracker

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.pedant.SweetAlert.SweetAlertDialog
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.Response.Listener
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.postData
import kotlinx.android.synthetic.main.activity_post_to_api.*
import org.json.JSONException


class MainActivity : AppCompatActivity() {
    private val mRecyclerView: RecyclerView? = null
    private var mExampleAdapter: CoronaAdapter? = null
    private var mExampleList: ArrayList<CoronaModel>? = null
    private var mRequestQueue: RequestQueue? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //setting my list items for my recycler view
//        val listRecycler = generateDummyList()
        //ref the recycler view widget and set the adapter to it
//        coronaRecycler.adapter = CoronaAdapter(this,listRecycler)
        //give our recycler items a view group
        coronaRecycler.layoutManager = LinearLayoutManager(this)
        //set a fixed item size
        coronaRecycler.setHasFixedSize(true)

        mExampleList = ArrayList()

        mRequestQueue = Volley.newRequestQueue(this)
        if (isNetworkConnected()) {
            fetchData();

        } else {
            Toast.makeText(applicationContext, "Internet not connected", Toast.LENGTH_LONG).show()
        }


        postData.setOnClickListener {
            val intent = Intent(this,PostToApi::class.java)
            startActivity(intent)
        }

    }

//    private fun fetchData(){
//        val url = "https://api.covid19api.com/summary"
//
//        //Request a string response from the URL resource
//
//        //Request a string response from the URL resource
//        val stringRequest = StringRequest(
//            Request.Method.GET, url,
//            object :  Listener<String> {
//                override fun onResponse(response: String) {
//                    // Display the response string.
//                    Log.d("response","response data is $response")
//                }
//            }, object : Response.ErrorListener {
//                override fun onErrorResponse(error: VolleyError?) {
//                    Log.d("response","response data is $error")
//                }
//            })
//
//        //Instantiate the RequestQueue and add the request to the queue
//
//        //Instantiate the RequestQueue and add the request to the queue
//        val queue = Volley.newRequestQueue(applicationContext)
//        queue.add(stringRequest)
//    }


    private fun fetchData(){
        val loadingDialog = SweetAlertDialog(this@MainActivity, SweetAlertDialog.PROGRESS_TYPE)
        loadingDialog.setTitleText("Loading ...") //Processing your request
        loadingDialog.setCancelable(true)
        loadingDialog.setCanceledOnTouchOutside(false)
        loadingDialog.show()
        val url = "https://api.covid19api.com/summary"
        val request = JsonObjectRequest(Request.Method.GET, url, null,
                Listener { response ->
                    loadingDialog.dismiss()
                    try {
                        val jsonArray = response.getJSONArray("Countries")
                        Log.d("array","response is $jsonArray")
                        for (i in 0 until jsonArray.length()) {
                            val hit = jsonArray.getJSONObject(i)
                            val id = hit.getString("ID")
                            val countryName = hit.getString("Country")
                            val countryNewCases = hit.getInt("NewConfirmed")
                            val countryTotalCases = hit.getInt("TotalConfirmed")
                            val countryNewRec = hit.getInt("NewRecovered")
                            val countryTotalRec = hit.getInt("TotalRecovered")
                            val countryNewDeath = hit.getInt("NewDeaths")
                            val countryTotalDeath = hit.getInt("TotalDeaths")
                            val infoDate = hit.getString("Date")
                            mExampleList!!.add(CoronaModel(id,countryName,countryNewCases,countryTotalCases,countryNewRec,countryTotalRec,countryNewDeath,countryTotalDeath,infoDate))
                        }
                        //adding model data to adapter
                        mExampleAdapter = mExampleList?.let { CoronaAdapter(this@MainActivity, it) }
                        coronaRecycler!!.adapter = mExampleAdapter
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener {
            error -> error.printStackTrace()
        })
        mRequestQueue!!.add(request)
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            connectivityManager.activeNetwork
        } else {
            TODO("VERSION.SDK_INT < M")
        }
        val networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)
        return networkCapabilities != null && networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
    }


//    private fun generateDummyList(): List<CoronaModel> {
//
//        //arraylist to add data
//        val listItems  = ArrayList<CoronaModel>()
//
//        listItems.add(
//            CoronaModel(
//                1,
//                "Afghanistan",
//            "12",
//                "20",
//            "22",
//                "23",
//                "30",
//            "54"
//            )
//        )
//
//        listItems.add(
//            CoronaModel(
//                2,
//                "Brazil",
//                "12",
//                "20",
//                "22",
//                "23",
//                "30",
//                "54"
//            )
//        )
//
//        return listItems
//    }
}