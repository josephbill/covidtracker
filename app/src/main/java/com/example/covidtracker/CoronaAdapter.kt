package com.example.covidtracker

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.card_item.view.*

class CoronaAdapter(private var context: Context, private var itemList: List<CoronaModel>)  : RecyclerView.Adapter<CoronaAdapter.RecyclerViewHolder>() {

    lateinit var filteredList: MutableList<CoronaModel>
    private val exampleList: List<CoronaModel>? = null
    private val exampleListFull: List<CoronaModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context).inflate(R.layout.card_item,parent,false)
        return RecyclerViewHolder(inflater)

    }

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: CoronaAdapter.RecyclerViewHolder, position: Int) {
        val items  = itemList[position]

        //setting data
        holder.textCountry.text = items.countryName
        holder.textNewCases.text = "New Cases: " + items.countryNewCases
        holder.textTotalCases.text = "Total Cases: " + items.countryTotalCases
        holder.textNewRec.text = "New Recoveries: " + items.countryNewRec
        holder.textTotalRec.text = "Total Recoveries: " + items.countryTotalRec
        holder.textNewDeath.text = "New Death: " + items.countryNewDeath
        holder.textTotalDeath.text = "Total Death: " +items.countryTotalDeath
        holder.textDate.text = "Last Update: " +items.date


    }




    //creating view holder class to enable me to ref the views inside the recycled item
    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val textCountry: TextView = itemView.coronaCountry
        val textNewCases: TextView = itemView.coronaNewCases
        val textTotalCases: TextView = itemView.coronaTotalCases
        val textNewRec: TextView = itemView.coronaNewRec
        val textTotalRec: TextView = itemView.coronaTotalRec
        val textNewDeath: TextView = itemView.coronaNewDeath
        val textTotalDeath: TextView = itemView.coronaTotalDeath
        val textDate: TextView = itemView.infoDate
    }



    }



