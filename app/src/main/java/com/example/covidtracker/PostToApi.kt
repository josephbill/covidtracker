package com.example.covidtracker

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.activity_post_to_api.*
import org.json.JSONObject


class PostToApi : AppCompatActivity() {
    var imageSelected: String = ""
    var texta: String = ""
    var textb: String = ""
    private val pickImage = 100
    private var imageUri: Uri? = null
    private var uriString: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_to_api)
        postData.setOnClickListener {
            pickUsersContent()

        }
        
        updateData.setOnClickListener { 
            updateDetails()
        }

        deleteData.setOnClickListener {
            deleteId()
        }


    }

    private fun deleteId() {
        //capture users id , either from text field or user id for app account
        val userId= "6"
        deleteUsingAPI(userId)
    }

    private fun deleteUsingAPI(userId: String) {
        var url = "https://postman-echo.com/delete?" + userId

        val request = StringRequest(
            Request.Method.DELETE, url,
            { response -> // response
                try {
                    Log.d("message", "Response: $response")
                } catch (e: Exception) {
                    Log.d("message", "Exception: $e")
                }
            }
        ) {
            // error.
            Log.d("message", "Volley error: $it")

        }

        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
    }

    private fun updateDetails() {
        //capture users id , either from text field or user id for app account
        val userId= "6"
        texta = textaa.text.toString()
        textb = textba.text.toString()
        updatetoAPi(userId, texta, textb)
    }

    private fun updatetoAPi(userId: String, texta: String, textb: String) {
        val url = "https://postman-echo.com/put?" + userId
        //update params
        //form fields and values
        val params = HashMap<String, String>()
        params["parameterfield"] = texta
        params["anotherparamfield"] = textb
        val jsonObject = JSONObject(params as Map<*, *>)

        //volley put request with parameters
        val request = JsonObjectRequest(
            Request.Method.PUT,
            url,
            jsonObject,
            Response.Listener { response ->
                // Process the json
                try {
                    Log.d("message", "Response: $response")
                } catch (e: Exception) {
                    Log.d("message", "Exception: $e")
                }
            },
            Response.ErrorListener {
                // Error in request
                Log.d("message", "Volley error: $it")
            })

        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)

    }

    fun postData(view: View){
        //using image picker to select image
        ImagePicker.with(this)
                .crop()	    			//Crop image(Optional), Check Customization for more option
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                .start()
    }

    private fun pickUsersContent() {
        //select users content
        //pick text
        texta = textaa.text.toString()
        textb = textba.text.toString()
        submittoApi(texta, textb)


        Log.d("details", " selected data is " + textb + " " + texta + imageUri)

    }

    private fun submittoApi(texta: String, textb: String) {
        val url = "https://postman-echo.com/post"

        // Post parameters
        // Form fields and values
        val params = HashMap<String, String>()
        params["foo1"] = texta
        params["foo2"] = textb
        val jsonObject = JSONObject(params as Map<*, *>)

        // Volley post request with parameters
        val request = JsonObjectRequest(Request.Method.POST, url, jsonObject,
            Response.Listener { response ->
                // Process the json
                try {
                    Log.d("message", "Response: $response")
                } catch (e: Exception) {
                    Log.d("message", "Exception: $e")
                }

            }, Response.ErrorListener {
                // Error in request
                Log.d("message", "Volley error: $it")
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            imageUri = data?.data!!

            // Use Uri object instead of File to avoid storage permissions
            image1.setImageURI(imageUri)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    }


